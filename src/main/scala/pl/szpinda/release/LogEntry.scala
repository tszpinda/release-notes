package pl.szpinda.release

import scala.util.matching.Regex
import java.util.Date

@serializable
class LogEntry(logMsg: String) {

  @scala.reflect.BeanProperty
  var logMessage = logMsg

  @scala.reflect.BeanProperty
  var checkInDate:Date = null
  
  var issueNumber = 0

  var issueKey = ""
    
  @scala.reflect.BeanProperty
  var summary = ""

  @scala.reflect.BeanProperty
  var issueType = ""

  @scala.reflect.BeanProperty
  var status = ""

  def getIssueKey(): String = {
    if ("".equals(issueKey)) {
      val pattern = new Regex("""(^\w+-\d+)""", "key");
      val result = pattern.findFirstMatchIn(logMessage);
      if(!result.isEmpty){
    	  issueKey = result.get.group("key")
    	  val pattern2 = new Regex("""^\w+-(\d+)""", "no");
    	  issueNumber = pattern2.findFirstMatchIn(issueKey).get.group("no").toInt
      }
    }
    issueKey
  }

  def getIssueNumber() : Int = {
    if(issueNumber == 0)
    {
    	getIssueKey() 
    }
    issueNumber
  }
  override def hashCode = 41 * (41 + getIssueKey().hashCode)
  override def equals(other: Any) = other match {
    case that: LogEntry => this.getIssueKey == that.getIssueKey
    case _ => false
  }

}	