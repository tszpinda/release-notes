package pl.szpinda.release

import org.junit._
import Assert._
import java.util.ArrayList
/*
@Test
class FilterServiceTest {
  
  @Test
  def filterService() {
    var list = new ArrayList[LogEntry]
    list.add(new LogEntry("OSD-1234 - fix tab bug"))
    list.add(new LogEntry("OSD-1235 - fix tab bug 2 OSD-1234"))
    
    var fields = new ArrayList[String]
    fields.add("key")
    
    assertEquals(2, FilterService.filter("", list, fields).size())
    println("####### ####### ####### ####### ####### ####### #######")
    assertEquals(1, FilterService.filter("in(OSD-1234)", list, fields).size())
    assertEquals(1, FilterService.filter("in(OSD-1234,OSD-1233)", list, fields).size())
    assertEquals(2, FilterService.filter("in(OSD-1234,OSD-1235)", list, fields).size())
    assertEquals(0, FilterService.filter("in(OSD-123)", list, fields).size())
    assertEquals(0, FilterService.filter("in(OSD-12343)", list, fields).size())

    //notIn
    assertEquals(0, FilterService.filter("notIn(OSD-1234,OSD-1235)", list, fields).size())
    assertEquals(1, FilterService.filter("notIn(OSD-1234)", list, fields).size())
    assertEquals(1, FilterService.filter("notIn(OSD-1235)", list, fields).size())
    assertEquals(2, FilterService.filter("notIn(OSD-12352)", list, fields).size())

    //in & notIn
    assertEquals(1, FilterService.filter("notIn(OSD-1235) and in(OSD-1234)", list, fields).size())
    
    //like
    assertEquals(1, FilterService.filter("like(OSD-1235)", list, fields).size())
    assertEquals(2, FilterService.filter("like(^OSD-\\d+)", list, fields).size())
    
    assertEquals(1, FilterService.filter("notLike(OSD-1235)", list, fields).size())
    assertEquals(0, FilterService.filter("notLike(^OSD-\\d+)", list, fields).size())
    
    //
    assertEquals(1, FilterService.filter("like(^OSD-\\d+) and in(OSD-1234)", list, fields).size())
  }
  
}

@Test
class FilterQueryTest {
    
	@Test 
    def filterInQuery() = {
    	var filter = new FilterInQuery("OSD-123,OSD-239,OSS-129")
    	assertTrue("not found", filter.matches("OSD-123"))
    	assertFalse("found", filter.matches("OSS-123"))
    	assertTrue("not found", filter.matches("OSD-239"))
    	assertFalse("found", filter.matches("OSD-133"))
    }
	
	@Test 
    def filterNotInQuery() = {
    	var filter = new FilterNotInQuery("OSD-123,OSD-239,OSS-129")
    	assertFalse("found", filter.matches("OSD-123"))
    	assertTrue("not found", filter.matches("OSS-123"))
    	assertFalse("found", filter.matches("OSD-239"))
    	assertTrue("not found", filter.matches("OSD-133"))
    }
	
	@Test 
    def filterLikeQuery() = {
		var filter = new FilterLikeQuery("\\s*?OSD-\\d+.*")
    	assertFalse("found", filter.matches("OSS-123"))
    	assertTrue("not found", filter.matches("OSD-123-test"))
    	assertFalse("found", filter.matches(" OSS-239 - resolved OSD-234 by it"))
    	assertTrue("not found", filter.matches(" OSD-1334 - given to"))
    }

	@Test 
    def filterNotLikeQuery() = {
    	var filter = new FilterNotLikeQuery("\\s*?OSD-\\d+.*")
    	assertTrue("not found", filter.matches("OSS-123"))
    	assertFalse("found", filter.matches("OSD-123-test"))
    	assertTrue("not found", filter.matches(" OSS-239 - resolved OSD-234 by it"))
    	assertFalse("found", filter.matches(" OSD-1334 - given to"))
	}	
	
}
*/