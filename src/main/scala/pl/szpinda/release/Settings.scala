package pl.szpinda.release

import org.slf4j.LoggerFactory

@serializable
class Settings(svnUrl1: String, svnUsername1: String, svnPasswd1: String,
               jiraUrl1: String, jiraUsername1: String, jiraPasswd1: String) {
  def svnUrl = svnUrl1

  def svnUsername = svnUsername1

  def svnPassword = svnPasswd1

  def jiraUrl = jiraUrl1

  def jiraUsername = jiraUsername1

  def jiraPassword = jiraPasswd1

  val logger = LoggerFactory.getLogger(classOf[Settings])
  logger.debug("svnUrl: {}, jiraUrl: {}", svnUrl, jiraUrl)
}
