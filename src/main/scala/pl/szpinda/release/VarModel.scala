package pl.szpinda.release

final object VarModel {
  def apply[T](getter: => T) = new VarModel[T](getter, null)
  def apply[T](getter: => T, setter: T => Unit) = new VarModel[T](getter, setter)
}

import org.apache.wicket.model.IModel;

@serializable
@SerialVersionUID(1L)
final class VarModel[T](getter: => T, setter: T => Unit) extends IModel[T] {
  def getObject: T = getter
  def setObject(v: T) = {
    if (setter == null)
      throw new UnsupportedOperationException("Tried to set readonly model!")
    setter(v)
  }
  val detach = ()
}