package pl.szpinda.release

;

import scala.collection.mutable.HashSet;


import java.util.ArrayList;
import scala.util.control.Breaks._;
import scala.collection.mutable.ListBuffer;

object FilterService {

  def filter(searchFilter: SearchFilter, entries: ArrayList[LogEntry]): ArrayList[LogEntry] = {
    if (searchFilter == null || searchFilter.isEmpty()) {
      return entries;
    }
    var list = new ArrayList[LogEntry]
    var filter = new Filter(searchFilter)

    var i = entries.listIterator;
    while (i.hasNext()) {
      var e = i.next;
      println(" entry:" + e.getIssueKey())
      if (filter.matches(e)) {
        println(" entry matched " + e.getIssueKey())
        list.add(e)
      } else {
        println(" entry not matched " + e.getIssueKey())
      }
    }
    println("entries matched: " + list.size())
    list
  }

}

class Filter(searchFilter: SearchFilter) {

  var valid = false
  var parsed = false
  var filters: ListBuffer[FilterQuery] = new ListBuffer[FilterQuery]
  var errors: ListBuffer[String] = new ListBuffer[String]

  val availableFilters = {
    var f = new ListBuffer[FilterQuery]
    f += new FilterInQuery("", "")
    f += new FilterNotInQuery("", "")
    f += new FilterLikeQuery("", "")
    f += new FilterNotLikeQuery("", "")
    f.toList
  }

  def matches(entry: LogEntry): Boolean = {
    if (!parsed) {
      parse()
    }
    var matches = true
    println("filters: " + filters)
    if (filters.size > 0) {
      breakable {
        filters.foreach(f => {
          println("filter: '" + f.name() + "' matches " + entry.getLogMessage() + ": " + f.matches(entry.getLogMessage()));

          f.field() match {
            case "key" => {
              if (!f.matches(entry.getIssueKey())) {
                matches = false
              }
            }
            case "issueType" => {
              if (!f.matches(entry.getIssueType())) {
                matches = false
              }
            }
            case "status" => {
              if (!f.matches(entry.getStatus())) {
                matches = false
              }
            }
            case _ => throw new IllegalArgumentException("No filter fields specified")

          }
          if (!matches) {
            break
          }
        })
      }
    } else {
      println("filters: emtpy")
      matches = true
    }

    println("matches: " + matches)
    matches
  }

  private def parse(): Unit = {
    parsed = true
    try {

      if (!searchFilter.issueType.isEmpty()) {
        filters += getFilterByName(searchFilter.issueType, "issueType")
      }
      if (!searchFilter.key.isEmpty()) {
        filters += getFilterByName(searchFilter.key, "key")
      }
      if (!searchFilter.status.isEmpty()) {
        filters += getFilterByName(searchFilter.status, "status")
      }
      valid = true
    } catch {
      case e: Exception => {
        throw e
      }
    }
  }

  def getFilterByName(filterString: String, field: String): FilterQuery = {
    var filterType = "in"
    filterType = filterString.substring(0, filterString.indexOf("("))
    var query = filterString.substring(filterString.indexOf("(") + 1, filterString.length() - 1)
    filterType match {
      case "in" => new FilterInQuery(query, field)
      case "notIn" => new FilterNotInQuery(query, field)
      case "like" => new FilterLikeQuery(query, field)
      case "notLike" => new FilterNotLikeQuery(query, field)
      case _ => throw new IllegalArgumentException("Filter not found for: '" + filterString + "'")
    }
  }
}

abstract class FilterQuery(filter: String, afield: String) {

  var filterText = {
    println("filterText: " + filter)
    filter
  }
  var valid = false
  var parsed = false
  var errors = new ArrayList[String]

  def field(): String = {
    afield
  }

  def matches(text: String): Boolean

  def name(): String

  override def toString(): String = {
    name()
  }
}

class FilterInQuery(filter: String, field: String) extends FilterQuery(filter: String, field: String) {

  var arr = filter.split(",");
  var filters = new HashSet[String]
  arr.foreach(i => filters.add(i))

  override def matches(text: String): Boolean = {
    print("accepted: ");
    arr.foreach(a => print(a) + ",")
    println(" is text ok?: " + text + " = " + filters.contains(text))
    filters.contains(text)
  }

  override def name = "in"
}

class FilterNotInQuery(filter: String, field: String) extends FilterInQuery(filter: String, field: String) {
  override def matches(text: String): Boolean = {
    !filters.contains(text)
  }

  override def name = "notIn"
}

class FilterLikeQuery(regexText: String, field: String) extends FilterQuery(regexText: String, field: String) {
  override def matches(text: String): Boolean = {
    var b = text.matches(regexText)
    println(text + ".matches.(" + regexText + ") = " + b)
    b
  }

  override def name = "like"
}

class FilterNotLikeQuery(regexText: String, field: String) extends FilterQuery(regexText: String, field: String) {
  override def matches(text: String): Boolean = {
    var b = !text.matches(regexText)
    println("!" + text + ".matches.(" + regexText + ") = " + b)
    b
  }

  override def name = "notLike"
}