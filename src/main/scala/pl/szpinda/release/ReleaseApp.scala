package pl.szpinda.release

import org.apache.wicket.protocol.http._
import org.apache.wicket.markup.html._
import org.apache.wicket.markup.html.basic.Label
import org.apache.wicket.request.mapper.parameter.PageParameters
import org.apache.wicket.ajax.markup.html.AjaxLink
import org.apache.wicket.ajax.AjaxRequestTarget
import org.apache.wicket.markup.repeater.data.DataView
import org.apache.wicket.markup.repeater.Item
import java.text.SimpleDateFormat
import org.slf4j.LoggerFactory

class ReleaseApp extends WebApplication {
  def getHomePage = classOf[HomePage]

  override def init(): Unit = {
    mountPage("view", classOf[HomePage])
    mountPage("admin", classOf[AdminPage])
    SettingProvider.details()
  }

}

class AdminPage(parameters: PageParameters) extends WebPage(parameters) {

  var cacheCount = new Label("inCache", VarModel({
    JiraService.cacheSize()
  }))
  add(cacheCount.setOutputMarkupId(true))
  add(new AjaxLink[Void]("clear") {
    def onClick(target: AjaxRequestTarget): Unit = {
      LogService.clearCache()
      JiraService.clearCache()
      target.add(cacheCount)
    }
  })
}

@serializable
class SearchFilter {
  var startRev = -10
  var endRev = -1

  var status = ""
  var key = ""
  var issueType = ""

  def isEmpty(): Boolean = {
    status.trim() == "" && key.trim() == "" && issueType.trim() == ""
  }
}


class HomePage(parameters: PageParameters) extends WebPage(parameters) {

  val logger = LoggerFactory.getLogger(classOf[HomePage])


  var keyParam = parameters.get("key");
  var statusParam = parameters.get("status");
  var typeParam = parameters.get("type");
  var fieldsParam = parameters.get("fields");
  var startRevParam = parameters.get("startRev");
  var endRevParam = parameters.get("endRev");
  var clearCacheParam = parameters.get("clearCache");

  var startRev = -10
  var endRev = -1
  if (!startRevParam.isEmpty()) {
    startRev = startRevParam.toInt()
  }
  if (!endRevParam.isEmpty()) {
    endRev = endRevParam.toInt()
  }
  if (clearCacheParam.isEmpty() || clearCacheParam.toBoolean) {
    LogService.clearCache();
    JiraService.clearCache;
    logger.info("cache clear")

  }

  var queryValid = (startRev != -10)

  var key = ""
  if (!keyParam.isEmpty()) {
    key = keyParam.toString()
  }
  var status = ""
  if (!statusParam.isEmpty()) {
    status = statusParam.toString()
  }
  var issueType = ""
  if (!typeParam.isEmpty()) {
    issueType = typeParam.toString()
  }
  logger.info("Query: key: " + key + " issueType:" + issueType + " status:" + status)

  var searchModel = VarModel({
    var filter = new SearchFilter();
    filter.endRev = endRev;
    filter.startRev = startRev;
    filter.key = key
    filter.status = status
    filter.issueType = issueType
    filter
  })
  var dateFormat = new SimpleDateFormat("dd-MM-yyyy");
  var results = new DataView[LogEntry]("results", new DataProvider(searchModel)) {

    override def populateItem(item: Item[LogEntry]): Unit = {
      val x = item.getModelObject();
      item.add(new Label("key", x.getIssueKey()))
      item.add(new Label("status", x.getStatus()))
      item.add(new Label("type", x.getIssueType()))
      item.add(new Label("summary", x.getSummary()))
      item.add(new Label("checkInDate", dateFormat.format(x.getCheckInDate())))
    }
  }
  add(results)
}

