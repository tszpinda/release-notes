package pl.szpinda.release

import collection.mutable
import scala.collection.immutable.List
import java.util
import org.slf4j.LoggerFactory
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;


import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter;

import org.springframework.web.client.RestTemplate
import com.trilead.ssh2.crypto.Base64
import util.ArrayList
import util.concurrent.ConcurrentHashMap
import scala.collection.JavaConversions._
;


/**
 * Created with IntelliJ IDEA.
 * User: tszpinda
 * Date: 01/09/2012
 * Time: 17:11
 * To change this template use File | Settings | File Templates.
 */


object JiraService {
  val logger = LoggerFactory.getLogger(getClass())
  val CACHE = new ConcurrentHashMap[String, LogEntry]()

  def cacheSize(): Int = {
    CACHE.size
  }

  def clearCache(): Unit = {
    CACHE.clear()
  }


  def load(logEntries: ArrayList[LogEntry]): Unit = {

    if (logEntries.isEmpty) {
      return
    }

    val entries = new mutable.HashMap[String, LogEntry]()
    var svnJiraKeys = List[String]()

    logEntries.foreach(log => {
      if (CACHE.contains(log.getIssueKey())) {
        val cached = CACHE.get(log.getIssueKey()).asInstanceOf[LogEntry]
        log.setIssueType(cached.getIssueKey())
        log.setStatus(cached.getStatus())
        log.setSummary(cached.getSummary())
      }

      //avoid calling jira if we already have info re this jira
      if (log.getStatus() == null || log.getStatus().isEmpty) {
        entries.put(log.getIssueKey(), log)
        svnJiraKeys = log.getIssueKey() :: svnJiraKeys
        logger.debug("cached: {}", log.getIssueKey())
      }
    })


    if (!entries.isEmpty) {
      val details = SettingProvider.details()
      val issueMap: mutable.HashMap[String, JiraIssue] = new mutable.HashMap[String, JiraIssue]()

      //todo Parallel Processing
      svnJiraKeys.foreach(jiraKey => {
        val template: RestTemplate = newRestTemplate()

        logger.info("JIRA request for : {}", jiraKey)
        val jiraRequest = new JiraRequest()
        jiraRequest.add(jiraKey)

        val requestEntity: HttpEntity[_] = newRequestEntity(jiraRequest, details.jiraUsername, details.jiraPassword)

        try {
          val response = template.exchange(details.jiraUrl, HttpMethod.POST, requestEntity, classOf[JiraResponse])
          val issuesArrList: util.ArrayList[JiraIssue] = response.getBody().getIssues()

          val issues = issuesArrList.toList.map(_.asInstanceOf[JiraIssue])

          issues.foreach(i => {
            issueMap.put(i.getKey(), i)
          })
          logEntries.foreach(le => scala.actors.Futures.future({
            val jiraIssue: Option[JiraIssue] = issueMap.get(le.getIssueKey())

            jiraIssue match {
              case None => {
                //should be in the cache
                val cachedEntry: LogEntry = CACHE.get(le.getIssueKey())
                if (cachedEntry != null) {
                  le.setIssueType(cachedEntry.getIssueType())
                  le.setStatus(cachedEntry.getStatus())
                  le.setSummary(cachedEntry.getSummary())
                } else {
                  le.setSummary("JIRA - ERROR - Not found")
                }
              }
              case Some(issue) => {
                le.setSummary(issue.getFields().getSummary())
                le.setIssueType(issue.getFields().getIssuetype().getName())
                le.setStatus(issue.getFields().getStatus().getName())
                CACHE.put(le.getIssueKey(), le)
              }
            }
          }))
          /*
  logEntries.foreach(le => {
    val jiraIssue: Option[JiraIssue] = issueMap.get(le.getIssueKey())

    jiraIssue match {
      case None => {
        //should be in the cache
        val cachedEntry: LogEntry = CACHE.get(le.getIssueKey())
        if (cachedEntry != null) {
          le.setIssueType(cachedEntry.getIssueType())
          le.setStatus(cachedEntry.getStatus())
          le.setSummary(cachedEntry.getSummary())
        } else {
          le.setSummary("JIRA - ERROR - Not found")
        }
      }
      case Some(issue) => {
        le.setSummary(issue.getFields().getSummary())
        le.setIssueType(issue.getFields().getIssuetype().getName())
        le.setStatus(issue.getFields().getStatus().getName())
        CACHE.put(le.getIssueKey(), le)
      }
    }
  })
          */
        } catch {
          case e: Exception => logger.error("Error processing JIRA request for: {}, error: {}", jiraKey, e.getMessage)
        }

      })

    }

    logEntries.sortBy(e => {
      (e.getCheckInDate(), e.getCheckInDate())
    })
  }

  def newRestTemplate(): RestTemplate = {
    val template = new RestTemplate()
    val converters = List(new MappingJacksonHttpMessageConverter())
    template.setMessageConverters(converters)
    template
  }

  def newRequestEntity(requestObject: Object, username: String, password: String): HttpEntity[_] = {
    var requestEntity = new HttpEntity[Object](requestObject, newHttpHeaders(username, password))
    requestEntity
  }

  def newHttpHeaders(username: String, password: String): HttpHeaders = {
    val headers = new HttpHeaders()
    var auth = username + ":" + password
    val encodedAuth = Base64.encode(auth.getBytes())
    val authHeader = "Basic " + new String(encodedAuth)
    headers.set("Authorization", authHeader);
    headers.setContentType(MediaType.APPLICATION_JSON);

    headers
  }
}