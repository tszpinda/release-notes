package pl.szpinda.release

import java.util.Properties
import org.slf4j.LoggerFactory

object SettingProvider {

  val logger = LoggerFactory.getLogger(SettingProvider.getClass)


  def details(): Settings = {

    var props = new Properties()
    var fileConfig = System.getProperty("rnconfig");
    if (fileConfig == null) {
      var defaultFilename = "release-notes.properties"
      var tomcatConfig = System.getProperty("TOMCAT_HOME")

      if (System.getProperty("TOMCAT_HOME") == null) {
        logger.debug("TOMCAT_HOME - not set trying location: {} ", System.getProperty("user.home"))
        fileConfig = System.getProperty("user.home") + "/" + defaultFilename
      } else {
        logger.debug("TOMCAT_HOME - set looking for properties in: {} ", System.getProperty("TOMCAT_HOME"))
        fileConfig = System.getProperty("TOMCAT_HOME") + "/" + defaultFilename
      }
    } else {
      logger.debug("using 'rnconfig' property to load config data")
    }
    logger.info("Loading properties from: " + fileConfig)
    var input = new java.io.FileInputStream(fileConfig);
    props.load(input);
    input.close();

    val conn = new Settings(
      props.getProperty("svn.url"),
      props.getProperty("svn.username"),
      props.getProperty("svn.password"),
      props.getProperty("jira.url"),
      props.getProperty("jira.username"),
      props.getProperty("jira.password"));

    conn
  }

}