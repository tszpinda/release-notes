import org.openqa.selenium.remote.{RemoteWebDriver, DesiredCapabilities}
import org.openqa.selenium.support.{FindBy, PageFactory}
import org.openqa.selenium.{WebElement, WebDriver}
import org.scalatest.junit.JUnitSuite
import org.scalatest.junit.ShouldMatchersForJUnit
import java.net.URL
import org.junit.{After, Test, Before}

/**
 * Created with IntelliJ IDEA.
 * User: tszpinda
 * Date: 05/09/2012
 * Time: 20:35
 * To change this template use File | Settings | File Templates.
 */
class ViewTest extends JUnitSuite with ShouldMatchersForJUnit {

  var url = "http://szpinda.pl/release-notes/view?startRev=4450&endRev=4460&clearCache=false"
  var adminUrl = "http://szpinda.pl/release-notes/admin"
  var webdriver: WebDriver = _

  @FindBy(linkText = "Clear")
  var clearLink: WebElement = _

  @FindBy(css = "table tr:nth-child(1) > td:nth-child(1)")
  var issueKeyRow1: WebElement = _

  @FindBy(css = "table tr:nth-child(1) > td:nth-child(3)")
  var issueTypeRow1: WebElement = _

  @Before def initialize() {
    val capabilities = DesiredCapabilities.firefox();
    //capabilities.setCapability("version", "5.0");
    //capabilities.setCapability("platform", Platform.MAC);
    val url = new URL("http://tszpinda:60b7a6dd-8a6e-4416-91bf-f0421c513498@ondemand.saucelabs.com:80/wd/hub")
    webdriver = new RemoteWebDriver(url, capabilities);

    PageFactory.initElements(webdriver, this)
    //webdriver = new FirefoxDriver()
    //webdriver.navigate().to(adminUrl)
    //clearLink.click()
  }

  @After def tearDown() {
    webdriver.quit()
  }

  @Test def view() {
    webdriver.navigate().to(url)
    val issueKey = issueKeyRow1.getText()
    assert(issueKey === "OSTECH-8")
    val issueType = issueTypeRow1.getText()
    assert(issueType === "Task")
  }

}
