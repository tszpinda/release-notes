package pl.szpinda.release

import org.codehaus.jackson.annotate.JsonIgnoreProperties
import java.util.ArrayList

@serializable
@JsonIgnoreProperties(ignoreUnknown = true)
class JiraResponse {
  @scala.reflect.BeanProperty
  var issues = new ArrayList[JiraIssue]
  
  override def toString(): String = {
    "" + issues
  }
}

@serializable
@JsonIgnoreProperties(ignoreUnknown = true)
class JiraIssue {

  @scala.reflect.BeanProperty
  var fields = new Fields
  
  @scala.reflect.BeanProperty
  var key = ""
  
  override def toString(): String = {
    "\nkey:" + key + "\n fields:" + fields
  }
}

@serializable
@JsonIgnoreProperties(ignoreUnknown = true)
class Fields {

  @scala.reflect.BeanProperty
  var summary = ""

  //@scala.reflect.BeanProperty
  //var description = ""

  @scala.reflect.BeanProperty
  var issuetype = new IssueType

  @scala.reflect.BeanProperty
  var status = new Status

  override def toString(): String = {
    "\n  summary: "+summary + "\n  issuetype: " + issuetype + "\n  status: " + status
  }
}

@serializable
@JsonIgnoreProperties(ignoreUnknown = true)
class IssueType {

  @scala.reflect.BeanProperty
  var name = ""

  override def toString(): String = {
    name
  }

}

@serializable
@JsonIgnoreProperties(ignoreUnknown = true)
class Status {

  @scala.reflect.BeanProperty
  var name = ""

  override def toString(): String = {
    name
  }
}
