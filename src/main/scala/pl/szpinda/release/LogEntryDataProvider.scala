package pl.szpinda.release

import org.apache.wicket.model.IModel
import org.apache.wicket.extensions.markup.html.repeater.util.SortableDataProvider
import java.util.ArrayList

class DataProvider[LogEntry](searchModel: IModel[SearchFilter]) extends SortableDataProvider[LogEntry] {

  var results: ArrayList[LogEntry] = null

  override def iterator(first: Int, count: Int): java.util.Iterator[LogEntry] = {
    // In this example the whole list gets copied, sorted and sliced; in real applications typically your database would deliver a sorted and limited list

    // Get the data

    // Sort the data
    //Collections.sort(newList, comparator);

    // Return the data for the current page - this can be determined only after sorting
    loadResults()

    return results.subList(first, first + count).iterator();
  }

  override def model(log: LogEntry): IModel[LogEntry] = {
    VarModel({
      log
    })
  }

  override def size(): Int = {
    loadResults()
    results.size();
  }

  def loadResults(): Unit = {
    if (results == null) {
      var logs = LogService.logs(SettingProvider.details(), searchModel.getObject().startRev, searchModel.getObject().endRev)

      JiraService.load(logs)

      results = FilterService.filter(searchModel.getObject(), logs).asInstanceOf[ArrayList[LogEntry]]
    }
  }

  override def detach(): Unit = {
    results = null
  }

}