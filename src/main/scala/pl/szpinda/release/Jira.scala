package pl.szpinda.release

// cobbled together using the internets (mostly stackoverflow)
// http://stackoverflow.com/questions/5564074/scala-http-operations/5571782#5571782
// http://stackoverflow.com/questions/4883100/how-to-handle-http-authentication-using-httpurlconnection/4883890#4883890
// JIRA's REST docs: http://docs.atlassian.com/jira/REST/latest/ (note that I couldn't figure out how to login by posting application/json -- using crude https basic auth)

import java.io.{OutputStreamWriter, BufferedReader, InputStreamReader}
import java.net.{URLConnection, HttpURLConnection, URL}
import scala.collection.JavaConversions._
import scala.io.Source
import java.net.URLEncoder.encode

class Session(val username: String, val password: String, val userAgent: String = "", val encoding: String = "UTF-8", val requestTimeout: Int = 15000) {
  var cookies = Map[String, String]()

  def loadCookies(conn: URLConnection) = {
    for ((name, value) <- cookies) conn.setRequestProperty("Cookie", name + "=" + value)
  }

  def saveCookies(conn: URLConnection) = {
    conn.getHeaderFields.lift("Set-Cookie") match {
      case Some(cList) => cList foreach { c =>
        val (name,value) = c span { _ != '=' }
        cookies += name -> (value drop 1)
      }
      case None =>
    }
  }
  
  def setupConnection(conn: URLConnection) = {
    conn.setConnectTimeout(requestTimeout)
    conn.setRequestProperty("User-Agent", userAgent)
  // conn.setChunkedStreamingMode(0)

    val userPassword = username + ":" + password
    conn.setRequestProperty("Authorization", "Basic " + (new sun.misc.BASE64Encoder()).encode(userPassword.getBytes()))
  }

  def doHttp(url: String, method: String = "GET", outputData: Option[String] = None, contentType: String = ""): String = {
    val conn = (new URL(url)).openConnection.asInstanceOf[HttpURLConnection]

    setupConnection(conn)
    conn.setRequestMethod(method)
    if(contentType nonEmpty) conn.setRequestProperty("Content-Type", contentType)

    loadCookies(conn)

    conn.setDoOutput(outputData nonEmpty) // doInput is true by default

    conn.connect()

    for(data <- outputData) yield {
      val wr = new OutputStreamWriter(conn.getOutputStream())
      wr.write(data)
      wr.flush
    }

    saveCookies(conn)

    val res = Source.fromInputStream(conn.getInputStream).getLines().mkString
    // println("res: " + conn.getResponseCode)
    conn.disconnect()
    res
  }
}

object JIRA {
  import scala.util.parsing.json._
  object JO { def apply(elems: (String, Any)*) = new JSONObject(Map(elems :_*)) }

  def parsePossibleTransitions(in: JSONType): List[Transition] = {
    in match { 
      case JSONObject(m) => (m map { case (id, JSONObject(trans)) =>
        def getRequiredFieldNames(in: Any) = in match { case JSONArray(fields) => fields flatMap {case JSONObject(obj) if obj("required").toString.toBoolean => Some(obj("id").toString) case _ => None } }
        var idInt = augmentString(id.toString()).toInt;
        new Transition(idInt, trans("name").toString, getRequiredFieldNames(trans("fields")))
      }).toList
      case _ => List()
    }
  }

  case class Transition(id: Int, name: String, requiredFields: List[String]) {
    def jsonInst(comment:String, fieldValues: List[String]) = JO("transition" -> id.toString, "fields" -> JO(requiredFields.zip(fieldValues) :_*), "comment" -> comment)

// valid resolutions: (don't know how to get them programmatically, except for enumerating them from https://issues.scala-lang.org/rest/api/2.0.alpha1/resolution/1)
// "Fixed", "Won't Fix", "Duplicate", "Incomplete", "Cannot Reproduce", "Invalid"
    def canClose = name == "Close Issue"
    def mkClose(comment: String, resolution: String = "Fixed") = jsonInst(comment, List(resolution))

    def canReopen = name == "Reopen"
    def mkReopen() = jsonInst("", List())
  }
}

class JIRA(val session: Session, val baseUrl: String = "https://issues.scala-lang.org/rest/api/2.0.alpha1/") {
  import scala.util.parsing.json._
  import JIRA._

  def parseJson(str: String): JSONType = {
    val p = new Parser()
    p.root(new p.lexical.Scanner(str)) map {case jo: JSONType => jo } getOrElse JSONObject(Map())
  }

  object transitions {
    def apply(issue: String) = parsePossibleTransitions(parseJson(session.doHttp(baseUrl+"issue/"+issue+"/transitions")))
    def update(issue: String, trans: JSONObject) = session.doHttp(baseUrl+"issue/"+issue+"/transitions", "POST", Some(trans.toString), "application/json")
  }

  // when you get it wrong, you get an java.io.IOException: "Server returned HTTP response code: 500 for URL: [...]"
  def closeIssue(issue: String, comment: String, resolution: String = "Fixed") = transitions(issue) = transitions(issue)(0).mkClose(comment, resolution)
  def reopenIssue(issue: String) = transitions(issue) = transitions(issue)(0).mkReopen
}

// scala> object jira extends JIRA(new Session(XXX, YYYY))
// defined module jira
// 
// scala> jira.transitions("SI-4637")
// res0: List[JIRA.Transition] = List(Transition(2,Close Issue,List(resolution)))
// 
// scala> jira.closeIssue("SI-4637", "juuuust one more test", "Incomplete")
// res1: String = ""

// jira.transitions("SI-4637") exists (_.canReopen)
// jira.transitions("SI-4637") exists (_.canClose)
