package pl.szpinda.release

import org.codehaus.jackson.annotate.JsonIgnore

@serializable
class JiraRequest {

  @scala.reflect.BeanProperty
  var jql = "";
  
  @scala.reflect.BeanProperty
  var startAt = 0;
  
  @scala.reflect.BeanProperty
  var maxResults = 1000;
  
  @JsonIgnore
  var keys = ""

  def add(key : String) = {
    if(keys.equals(""))
    {
      keys = key;
    }else{
      keys = keys + "," + key
    }
    jql = "key in (" + keys + ")"
  }
}