package pl.szpinda.release

import org.tmatesoft.svn.core.SVNLogEntry
import org.tmatesoft.svn.core.SVNURL
import org.tmatesoft.svn.core.auth.ISVNAuthenticationManager
import org.tmatesoft.svn.core.io.SVNRepository
import org.tmatesoft.svn.core.io.SVNRepositoryFactory
import org.tmatesoft.svn.core.wc.SVNWCUtil
import scala.collection.JavaConversions._
import org.springframework.http.HttpEntity
import org.springframework.http.HttpHeaders
import org.springframework.http.HttpMethod
import org.springframework.http.MediaType
import org.springframework.http.ResponseEntity
import org.springframework.http.converter.HttpMessageConverter
import org.springframework.http.converter.json.MappingJacksonHttpMessageConverter
import org.springframework.web.client.RestClientException
import org.springframework.web.client.RestTemplate
import scala.collection.mutable.HashSet
import java.util.ArrayList
import java.util.LinkedList
import java.util.concurrent.ConcurrentHashMap
import java.util.Calendar
import org.slf4j.LoggerFactory

object LogService {

  val logger = LoggerFactory.getLogger(getClass())

  
  //cache
  val cache = new ConcurrentHashMap[Long, LogEntry]
  var cacheMinRevision = -1l
  var cacheMaxRevision = -1l

  def clearCache() : Unit = {
    cache.clear();
    cacheMinRevision = -1l
    cacheMaxRevision = -1l
  }
  
  def getLoadStartRev(startRevision: Long, cacheMinRevision: Long, cacheMaxRevision: Long): Long =
    {
      var loadRev = startRevision
      if (cacheMinRevision != -1) {
        if (startRevision >= cacheMinRevision) {
          loadRev = cacheMaxRevision
        }
      }
      loadRev
    }

  var lastLoadFromHead = Calendar.getInstance()
  def getLoadEndRev(endRevision: Long, cacheMinRevision: Long, cacheMaxRevision: Long): Long =
  {
      var loadRev = endRevision
      if (endRevision != -1 && cacheMaxRevision != -1) {
        if (endRevision <= cacheMaxRevision) {
          loadRev = cacheMinRevision
        }
      }
      
      //if we had cached and we trying to load up to
      //the HEAD then check if we did that in last 5 min already
      //and if so use cached version
      if(loadRev == -1 && cacheMaxRevision > 0)
	  {
        logger.debug("loadRev:" + loadRev + " :" +cacheMaxRevision)
	    val cacheLength = Calendar.getInstance()
	    cacheLength.setTime(lastLoadFromHead.getTime())
	    cacheLength.add(Calendar.MINUTE, 5)
	    
	    logger.debug("cacheLength: " + cacheLength.getTime() + "<" + lastLoadFromHead.getTime())
	    if(lastLoadFromHead.getTime().before(cacheLength.getTime()))
	    {
	      loadRev = cacheMaxRevision
	    }
	  }
      lastLoadFromHead = Calendar.getInstance()     
      loadRev
  }

  def logs(settings: Settings, startRevision: Long, endRevision: Long): ArrayList[LogEntry] = {

    var url = SVNURL.parseURIDecoded(settings.svnUrl);
    var repository = SVNRepositoryFactory.create(url);
    var authManager = SVNWCUtil.createDefaultAuthenticationManager(settings.svnUsername, settings.svnPassword);
    repository.setAuthenticationManager(authManager);
    var targetPaths = Array("");
    //changedPath if true then revision information will also include all changed paths per revision, otherwise not
    var changedPath = false;
    //If strictNode is true, copy history will not be traversed (if any exists) when harvesting the revision logs for each path.
    var strictNode = false;

    var results = new HashSet[LogEntry]

    cache.keySet().foreach(revision => {
      if (revision >= startRevision && (endRevision == -1 || revision <= endRevision)) {
        results.add(cache.get(revision))
        logger.debug("from cache rev:" + revision)
      }
    })

    var loadStartRevision = getLoadStartRev(startRevision, cacheMinRevision, cacheMaxRevision)
    var loadEndRevision = getLoadEndRev(endRevision, cacheMinRevision, cacheMaxRevision)
    
    logger.debug("load rev, start: " + loadStartRevision + " : " + loadEndRevision)
    //logger.debug("cacheRevision:" + cacheMinRevision + ":" + cacheMaxRevision)
    
    if (loadEndRevision == -1 || loadStartRevision < loadEndRevision) {
      if (loadStartRevision < cacheMinRevision || cacheMinRevision == -1) {
        cacheMinRevision = loadStartRevision
      }
      if (loadEndRevision > cacheMaxRevision) {
        cacheMaxRevision = loadEndRevision
      }
      
      logger.debug("Load from svn rev:" + loadStartRevision + ":" + loadEndRevision)
      var logEntries = repository.log(targetPaths, null, loadStartRevision, loadEndRevision, changedPath, strictNode);
      var list = logEntries.asInstanceOf[LinkedList[SVNLogEntry]]
      
      var resultsSet = new HashSet[String]
      list.iterator.toList.foreach(i => {
        var log = new LogEntry(i.getMessage())
        log.setCheckInDate(i.getDate());
        
        if (!log.getIssueKey().isEmpty()) {
          
          if(!resultsSet.contains(log.getIssueKey())){
        	logger.debug("added to cache rev:" + i.getRevision() + ":" + log.getIssueKey())
          	results.add(log)
          	cache.put(i.getRevision(), log)
          	resultsSet.add(log.getIssueKey())
          }else{
            var entry:Option[LogEntry] = results.findEntry(log)
            entry.get.setCheckInDate(i.getDate())
          }
        }
        if (loadEndRevision == -1) {
        	cacheMaxRevision = i.getRevision()
        }
      })
    }

    new ArrayList[LogEntry](results)
  }

}

